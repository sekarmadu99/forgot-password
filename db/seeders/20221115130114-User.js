'use strict';

const bcrypt = require('bcrypt');

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('Users', [
    {
      name: 'Sekar MK',
      email: 'sekarmadu99@gmail.com',
      password: await bcrypt.hash('secret', 10),
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Doni',
      email: 'doniwahyu56@gmail.com',
      password: await bcrypt.hash('secret', 10),
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Honestyan',
      email: 'honestyan0708@gmail.com',
      password: await bcrypt.hash('secret', 10),
      createdAt: new Date(),
      updatedAt: new Date()
    },
   ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Users', null, {});
  }
};
