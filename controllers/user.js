const { User } = require('../models');
const bcrypt = require('bcrypt');
const forgot_password = require('../utils/forgot-password');
const { response } = require('express');
const jwt = require('jsonwebtoken');

const {JWT_SIGNATURE_KEY} = process.env;

module.exports = {
    index: async (req, res, next) => {
        try {
            const usersGameData = await User.findAll({raw: true});
            return res.status(200).json({
                status: true,
                message: 'get all user success',
                data: usersGameData
            })
        } catch (err) {
            next(err);
        }
    },
    show: async (req, res, next) => {
        try {
            const {userId} = req.params;
            const userData = await User.findOne({where: {id: userId}});
            if(!userData) {
                return res.status(400).json({
                    status: false,
                    message: 'user not found',
                    data: null
                });
            }
            return res.status(200).json({
                status: true,
                message: 'get user success',
                data: userData.get()
            });
        } catch (err) {
            next(err);
        }
    },
    create: async (req, res, next) => {
        try {
            const {name, email, password} = req.body;
            
            const isExist = await User.findOne({where: {email: email}});
            if(isExist) {
                return res.status(409).json({
                    status: false,
                    message: 'email already used',
                    data: null
                });
            }

            const userData = await User.create({
                name,
                email,
                password: await bcrypt.hash(password, 10),
            });

            return res.status(201).json({
                status: true,
                message: 'create new user success',
                data: userData
            });
        } catch (err) {
            next(err);
        }
    },
    update: async (req, res, next) => {
        try {
            const {userId} = req.params;
            let {name, email, password} = req.body;

            const userData = await User.findOne({where: {id: userId}});
            if(!userData) {
                return res.status(400).json({
                    status: false,
                    message: 'user not found',
                    data: null
                });
            }

            if(!name) name = userData.name;
            if(!email) email = userData.email;
            if(!password) password = userData.password;

            const isUpdated = await User.update({
                name,
                email,
                password: await bcrypt.hash(password, 10)
            }, {
                where: {id: userId}
            });

            return res.status(200).json({
                status: true,
                message: 'update user success',
                data: isUpdated
            });
        } catch (err) {
            next(err);
        }
    },
    delete: async (req, res, next) => {
        try {
            const {userId} = req.params;

            const userData = await User.findOne({where: {id: userId}});
            if(!userData) {
                return res.status(400).json({
                    status: false,
                    message: 'user not found',
                    data: null
                });
            }

            const isDeleted = await User.destroy({
                where: {id: userId}
            });

            return res.status(201).json({
                status: true,
                message: 'delete user success',
                data: isDeleted
            });
        } catch (err) {
            next(err);
        }
    },
    forgot_password: async (req, res, next) => {
        try {
            const {email} = req.body;

            const user = await User.findOne({where: {email}});

            if(!user) {
                return res.status(400).json({
                    status: false,
                    message: 'email doesnt exist',
                    data: null,
                });
            }

            // generate payload
            payload = {
                id: user.id,
                name: user.name,
                email: user.email,
            };

            const token = jwt.sign(payload, JWT_SIGNATURE_KEY);

            await forgot_password.sendMail(email, '[Forgot Password]', `<a href='http://localhost:3000/users/reset-password?token=${token}'>click here to reset your password</a>`)

            return res.status(200).json({
                status: true,
                message: 'success',
                data: user,
            });
        } catch (err) {
            next(err);
        }
    },
    reset_password: async (req, res, next) => {
        try {
            const token = req.query.token;
            const { new_password, confirm_new_password } = req.body;

            if(new_password !== confirm_new_password) {
                return res.status(422).json({
                    status: false,
                    message: 'new password and confirm new password doesnt match'
                });
            }

            console.log('INI TOKEN' + token);

            const decoded = jwt.verify(token, JWT_SIGNATURE_KEY);


            const user = await User.findOne({ where: { id: decoded.id } });
            
            if (!user) {
                return res.status(400).json({
                    status: false,
                    message: 'user not found',
                    data: null,
                });
            }

            const isUpdated = await User.update({
                password: await bcrypt.hash(new_password, 10)
            }, {
                where: { id: decoded.id }
            });
            

            return res.status(200).json({
                status: true,
                message: 'update user success',
                data: isUpdated
            });
        } catch (err) {
            next(err);
        }
    }
}

/*
    endpoint forgot password -> http://localhost/forgot-password {email: user_email}
*/
// ambil user_email
// findOne by email
// generate token (user_id)
// kirim token dan url ganti password ke email user -> http://localhost/reset_password?token=


/*
    endpoint reset password -> http://localhost/reset-password?token=  {new_password}
*/
// ambil token dan body data
// extract/verify token -> dapatin payload { user_id}
// validasi apakah user_id ada di database
// bcrypt(password)
// update user password where user_id = payload.user_id
// success
