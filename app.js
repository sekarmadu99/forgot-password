require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const router = require('./routes');
const app = express();

const {HTTP_PORT} = process.env;

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(morgan('dev'));
app.use(router);

app.listen(HTTP_PORT, () => console.log('listening on port ' + HTTP_PORT));



// async function main() {
//     try {
//         await sendEmail('sekarmadukusumawardani@gmail.com', 'Test Mail', '<h1>Ini adalah isi email</h1>');
//     } catch (err) {
//         console.log(err.message);
//     }
// }

// main();