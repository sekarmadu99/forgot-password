const express = require('express');
const router = express.Router();
const c = require('../controllers');

router.get('/', c.user.index);
router.get('/:userId', c.user.show);
router.post('/', c.user.create);
router.put('/:userId', c.user.update);
router.delete('/:userId', c.user.delete);

router.post('/forgot-password', c.user.forgot_password);
router.post('/reset-password', c.user.reset_password);

module.exports = router;
